import json
import pika
import django
import os
import sys
from django.core.mail import send_mail

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    print(" Received %r" % body)

    send_mail(
        "Your presentation has been accepted",
        f'{data["presenter_name"]}, we are happy to tell you that your presentation {data["title"]} has been accepted.',
        "admin@conference.go",
        [data["presenter_email"]],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    print(" Received %r" % body)
    data = json.loads(body)
    send_mail(
        "Your presentation has been rejected",
        f'{data["presenter_name"]}, we are sad to tell you that your presentation {data["title"]} has been rejected',
        "admin@conference.go",
        [data["presenter_email"]],
        fail_silently=False,
    )


while True:

    def main():
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()

        channel.queue_declare(queue="presentation_approval")
        channel.basic_consume(
            queue="presentation_approval",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.queue_declare(queue="presentation_rejection")
        channel.basic_consume(
            queue="process_rejection",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        print("[*] Waiting for emails. To exit press CTRL+C")
        channel.start_consuming()

    if __name__ == "__main__":
        try:
            main()
        except KeyboardInterrupt:
            print("Interrupted")
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)
# except AMQPConnectionError:
#     print("Could not connect to RabbitMQ")
#     time.sleep(2.0)
